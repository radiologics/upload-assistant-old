/*
 * upload-assistant: org.nrg.xnat.upload.io.ecat.EcatTrawler
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io.ecat;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.nrg.xnat.upload.ecat.EcatSession;
import org.nrg.ecat.MatrixDataFile;
import org.nrg.xnat.upload.io.Trawler;
import org.nrg.xnat.upload.data.Session;
import org.nrg.framework.io.EditProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class EcatTrawler implements Trawler {
    public EcatTrawler(final JComponent parent) {
        _parent = parent;
    }

    @Override
    public Collection<Session> trawl(final Iterator<File> files, final Collection<File> remaining, EditProgressMonitor pm) {
        final ListMultimap<String,MatrixDataFile> filesets = ArrayListMultimap.create();
        while (files.hasNext()) {
        	if (null != pm && pm.isCanceled()) {
				return new ArrayList<>();
			}
            final File f = files.next();
            logger.trace("checking {}", f);
            try {
                final MatrixDataFile mdf = new MatrixDataFile(f);
                filesets.put(mdf.getPatientID(), mdf);
            } catch (IOException e) {
                logger.debug(f + " is not an ECAT file", e);
                if (null != remaining) {
                    remaining.add(f);
                }
            }
        }

        logger.trace("found ECAT sessions: {}", filesets);
        final ArrayList<Session> sessions = Lists.newArrayList();
        for (final String label : Sets.newTreeSet(filesets.keySet())) { // add in sorted order
            sessions.add(new EcatSession(_parent, filesets.get(label)));
        }
        return sessions;
    }

    private static final Logger logger = LoggerFactory.getLogger(EcatTrawler.class);

    private final JComponent _parent;
}
