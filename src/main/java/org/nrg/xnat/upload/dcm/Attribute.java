/*
 * upload-assistant: org.nrg.xnat.upload.dcm.Attribute
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import java.util.Map;
import java.util.WeakHashMap;

public interface Attribute {
	class Simple implements Attribute {
		private final static Map<Integer,Attribute> attrs = new WeakHashMap<>();
		
		public static Attribute getInstance(final int tag) {
			final Attribute a = attrs.get(tag);
			if (null == a) {
				final Attribute na = new Simple(tag);
				attrs.put(tag, na);
				return na;
			} else {
				return a;
			}
		}
		
		private final int tag;
		
		private Simple(final int tag) { this.tag = tag; }
		
		public int hashCode() { return tag; }
		
		public boolean equals(final Object o) {
			return o instanceof Simple && tag == ((Simple)o).tag;
		}
	}
}
