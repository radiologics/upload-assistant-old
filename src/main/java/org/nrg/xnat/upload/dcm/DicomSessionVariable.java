/*
 * upload-assistant: org.nrg.xnat.upload.dcm.DicomSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.IndexedLabelValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.xnat.upload.application.UploadAssistant;
import org.nrg.xnat.upload.data.AbstractSessionVariable;
import org.nrg.xnat.upload.data.IndexedDependentSessionVariable;
import org.nrg.xnat.upload.data.SessionVariable;
import org.nrg.xnat.upload.data.ValueListener;
import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.util.Set;

public abstract class DicomSessionVariable extends AbstractSessionVariable implements ValueListener {
    public static SessionVariable getSessionVariable(final Variable variable, final DicomObject sample) {
        if (variable.getInitialValue() != null && variable.getInitialValue() instanceof IndexedLabelValue) {
            return new IndexedDependentSessionVariable(variable);
        }
        return new TextDicomVariable(variable, sample);
    }

    DicomSessionVariable(final Variable variable, final DicomObjectI sampleObject) {
        super(variable);
        _sampleObject = sampleObject;
    }

    @Override
    public String getDescription() {
        return StringUtils.defaultIfBlank(super.getDescription(), getName());
    }

    @Override
    public Value getValue() {
        final Value value = super.getValue();
        if (value != null) {
            return value;
        }
        final Value initialValue = super.getInitialValue();
        if (initialValue != null) {
            try {
                return new ConstantValue(initialValue.on(_sampleObject));
            } catch (ScriptEvaluationException e) {
                //non-ideal, but somewhat protects against HTTP issues in GetURL
                JOptionPane.showMessageDialog(UploadAssistant.getInstance(),
                                              Messages.getDialogFormattedMessage("dicomsessionvariable.seriouserror.message", e.getMessage()),
                                              Messages.getMessage("dicomsessionvariable.seriouserror.title"),
                                              JOptionPane.ERROR_MESSAGE);
                return null;
            }
        }
        return null;
    }

    public Set<Long> getTags() {
        final Value initialValue = getInitialValue();
        return null == initialValue ? Value.EMPTY_TAGS : initialValue.getTags();
    }

    public Set<Variable> getVariables() {
        final Value initialValue = getInitialValue();
        return null == initialValue ? Value.EMPTY_VARIABLES : initialValue.getVariables();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.ValueListener#hasChanged(org.nrg.upload.data.SessionVariable)
     */
    public void hasChanged(final SessionVariable variable) {
        fireHasChanged();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.ValueListener#isInvalid(org.nrg.upload.data.SessionVariable, java.lang.Object, java.lang.String)
     */
    public void isInvalid(SessionVariable variable, Object value, String message) {
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return super.toString() + "/" + getName() + " = " + (getValue() == null ? "[null]" : getValue().asString()) + " [init " + (getValue() == null ? "[null]" : getValue().asString()) + "]";
    }

    private final DicomObjectI _sampleObject;
}
