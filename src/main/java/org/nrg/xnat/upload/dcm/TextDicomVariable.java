/*
 * upload-assistant: org.nrg.xnat.upload.dcm.TextDicomVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.dcm4che2.data.DicomObject;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.variables.Variable;

final class TextDicomVariable extends DicomSessionVariable implements DocumentListener {
	TextDicomVariable(final Variable variable, final DicomObject sample) {
		this(variable, null, sample);
	}

	TextDicomVariable(final Variable variable, final String value, final DicomObject sample) {
		super(variable, DicomObjectFactory.newInstance(sample));
		_text = new JTextField(value);
		_text.getDocument().addDocumentListener(this);
	}

    /**
     * Overrides the base {@link super#setValue(String)} method to add updating the display value in accordance with the
     * change to the value.
     *
     * @param value The value to set.
     *
     * @return The previously set value of the variable.
     */
    @Override
    public String setValue(final String value) {
        super.setValue(value);
        return updateDisplayValue();
    }

    /*
	 * (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
	 */
	public void changedUpdate(final DocumentEvent e) {
		editTo(_text.getText());
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.xnat.upload.data.SessionVariable#getEditor()
	 */
    public JTextField getEditor() {
        return _text;
    }
		
	/*
	 * (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
	 */
	public void insertUpdate(final DocumentEvent e) {
		editTo(_text.getText());
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
	 */
	public void removeUpdate(final DocumentEvent e) {
		editTo(_text.getText());
	}
	
	public void refresh() {
		updateDisplayValue();
	}
	
	private final JTextField _text;
}
