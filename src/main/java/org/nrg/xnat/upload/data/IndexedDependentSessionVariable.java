/*
 * upload-assistant: org.nrg.xnat.upload.data.IndexedDependentSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.BasicVariable;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.Collections;
import java.util.Map;

public class IndexedDependentSessionVariable extends AbstractSessionVariable implements SessionVariable, DocumentListener {
    /**
     * @param name       Name of the session variable.
     * @param dependency The session variable on which this indexed session variable is dependent. This is used for
     *                   updating the value of the control.
     * @param format     A string format for composing the variable value.
     * @param validator  The object that validates and verifies the control value.
     */
    public IndexedDependentSessionVariable(final String name,
                                           final SessionVariable dependency,
                                           final String format,
                                           final ValueValidator validator) {
        super(new BasicVariable(name));

        _format = format;
        _validator = validator;
        _text = new JTextField();

        try {
            _text.setText(getValidName());
        } catch (ScriptEvaluationException e) {
            logger.error("Got a script exception", e);
            _text.setText("");
        }

        setDependency(dependency);
    }

    public IndexedDependentSessionVariable(final Variable variable) {
        super(variable);
        _format = null;
        _text = new JTextField(variable.getValue() != null ? variable.getValue().asString() : null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changedUpdate(final DocumentEvent e) {
        edit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getEditor() {
        return _text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Value getValue() {
        final Value value = super.getValue();
        if (value == null) {
            if (!_edited) {
                final Value initialValue = getInitialValue();
                if (initialValue != null) {
                    return initialValue;
                }
            }
            return new ConstantValue(_text.getText());
        }
        return value;
    }

    /**
     * Overrides the {@link AbstractSessionVariable#setValue(String) base setValue(String)} method to add updating the
     * display value in accordance with the change to the value.
     *
     * @param value The value to set.
     *
     * @return The previously set value of the variable.
     */
    @Override
    public String setValue(final String value) {
        super.setValue(value);
        return updateDisplayValue();
    }

    /**
     * Gets the {@link SessionVariable} on which this variable depends for its indexed value.
     *
     * @return The {@link SessionVariable} on which this variable depends.
     */
    public SessionVariable getDependency() {
        return _dependency;
    }

    /**
     * Sets the {@link SessionVariable} on which this variable depends for its indexed value.
     *
     * @param dependency The {@link SessionVariable} on which this variable depends.
     */
    public void setDependency(SessionVariable dependency) {
        _dependency = dependency;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertUpdate(final DocumentEvent e) {
        edit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isHidden() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        try {
            if (!_edited) {
                _text.setText(getValidName());
            } else {
                validate(_text.getText());
            }
            // setValue(_text.getText());
            fireHasChanged();
        } catch (InvalidValueException e) {
            logger.trace("Invalid value, ignoring change to " + _dependency.getName(), e);
        } catch (ScriptEvaluationException e) {
            logger.trace("Error in script, ignoring change to " + _dependency.getName(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeUpdate(final DocumentEvent e) {
        edit();
    }

    private void edit() {
        _edited = true;
        editTo(_text.getText());
    }

    private String getValidName() throws ScriptEvaluationException {
        int    i = 1;
        String name;
        if (_validator != null) {
            do {
                name = getNameFromSource(i++);
            } while (!_validator.isValid(name));
        } else {
            name = getNameFromSource(i);
        }
        return name;
    }

    private String getNameFromSource(final int index) throws ScriptEvaluationException {
        final String value = _dependency != null && _dependency.getValue() != null
                             ? _dependency.getValue().asString()
                             : null;
        return StringUtils.isNotBlank(_format)
               ? String.format(_format, value, index)
               : getValue().on(EMPTY);
    }

    private static final Map<Integer, String> EMPTY = Collections.emptyMap();

    private static final Logger logger = LoggerFactory.getLogger(IndexedDependentSessionVariable.class);

    private final String     _format;
    private final JTextField _text;

    private ValueValidator  _validator;
    private SessionVariable _dependency;

    private boolean _edited = false;
}
