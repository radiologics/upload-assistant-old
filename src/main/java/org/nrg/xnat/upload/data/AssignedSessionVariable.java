/*
 * upload-assistant: org.nrg.xnat.upload.data.AssignedSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import org.nrg.dicom.mizer.exceptions.MultipleInitializationException;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.BasicVariable;

import javax.swing.JLabel;

public final class AssignedSessionVariable extends AbstractSessionVariable {
    public AssignedSessionVariable(final String name, final String value, final boolean hidden) {
        super(name);
        super.setValue(value);
        _label = new JLabel(value);
        _hidden = hidden;
    }

    public AssignedSessionVariable(final String name, final String value) {
        this(name, value, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel getEditor() {
        return _label;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValueMessage() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isHidden() {
        return _hidden;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
    }

    /**
     * For this class, we don't allow setting the value and only set a value for it by calling the {@link
     * BasicVariable#setValue(String)} method in the constructor.
     *
     * @param value The value to be set.
     *
     * @throws UnsupportedOperationException This operation is not allowed for this implementation.
     */
    @Override
    public String setValue(final String value) {
        throw new UnsupportedOperationException();
    }

    /**
     * For this class, we don't allow setting the value and only set a value for it by calling the {@link
     * #setInitialValue(String)} method in the constructor.
     *
     * @param value The value to be set.
     *
     * @throws UnsupportedOperationException This operation is not allowed for this implementation.
     */
    @Override
    public void setValue(final Value value) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final Value value = getValue();
        return super.toString() + " " + getName() + " = " + (value == null ? "[null]" : value.asString());
    }

    private final JLabel  _label;
    private final boolean _hidden;
}
