/*
 * upload-assistant: org.nrg.xnat.upload.ui.SessionReviewPanel
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.ArrayUtils;
import org.nrg.framework.ui.TraceableGridBagPanel;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.net.XnatServer;
import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collections;
import java.util.List;

public final class XnatServerAuthenticationPanel extends TraceableGridBagPanel {
    public XnatServerAuthenticationPanel(final XnatServerAuthenticationPage parent, final UploadAssistantSettings settings, final boolean trace) {
        super(trace);

        _parent = parent;
        _settings = settings;

        _serverListModel = new XnatServerComboBoxModel(_settings);
        _serverList = new JComboBox<XnatServer>() {{
            setModel(_serverListModel);
            addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(final ItemEvent event) {
                    if (event.getStateChange() == ItemEvent.SELECTED) {
                        final XnatServer server = getSelectedXnatServer();
                        if (server != null) {
                            _settings.setCurrentXnatServer(server);
                            final char[] password = server.getPassword();
                            _password.setText(ArrayUtils.isNotEmpty(password) ? new String(password) : "");
                        }
                    }
                }
            });
        }};

        _create = new JButton(new ImageIcon(getClass().getResource("/images/file-added-16.png"))) {{
            setToolTipText(Messages.getMessage("xnatserverauthenticationpanel.controls.create"));
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    _parent.showConfigurationPanel();
                }
            });
        }};

        _edit = new JButton(new ImageIcon(getClass().getResource("/images/file-edit-16.png"))) {{
            setToolTipText(Messages.getMessage("xnatserverauthenticationpanel.controls.edit"));
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    _parent.showConfigurationPanel(_serverList.getItemAt(_serverList.getSelectedIndex()));
                }
            });
        }};

        _delete = new JButton(new ImageIcon(getClass().getResource("/images/file-delete-16.png"))) {{
            setToolTipText(Messages.getMessage("xnatserverauthenticationpanel.controls.delete"));
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent event) {
                    final String toBeDeleted = _settings.getCurrentXnatServer().getName();
                    _settings.deleteServer(toBeDeleted);
                    adjustPanelState();
                    _parent.validateContents(getParent(), event);
                }
            });
        }};

        final JPanel buttons = new JPanel(new FlowLayout()) {{
            add(_create);
            add(_edit);
            add(_delete);
        }};

        _password = new JPasswordField();
        final JLabel passwordLabel = new JLabel(Messages.getMessage("xnatserverauthenticationpanel.controls.password"));
        passwordLabel.setLabelFor(_password);

        add(_serverList, new GridBagConstraints() {{
            gridx = gridy = 0;
            gridwidth = 3;
            weightx = 1;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(buttons, new GridBagConstraints() {{
            gridx = 3;
            gridy = 0;
            gridwidth = 1;
            weightx = 0;
            anchor = GridBagConstraints.EAST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(passwordLabel, new GridBagConstraints() {{
            gridx = 0;
            gridy = 1;
            gridwidth = 1;
            weightx = 0;
            anchor = GridBagConstraints.LINE_START;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(_password, new GridBagConstraints() {{
            gridx = 1;
            gridy = 1;
            gridwidth = 2;
            weightx = 1;
            anchor = GridBagConstraints.LINE_START;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});

        adjustPanelState();
    }

    public XnatServer getSelectedXnatServer() {
        return _serverList.getItemAt(_serverList.getSelectedIndex());
    }

    public char[] getPassword() {
        return _password.getPassword();
    }

    public String validateContents() {
        final XnatServer server = getSelectedXnatServer();
        if (server == null) {
            return Messages.getMessage("xnatserverauthenticationpanel.selectserver");
        }
        if (ArrayUtils.isEmpty(getPassword())) {
            return Messages.getMessage("xnatserverauthenticationpanel.enterpassword");
        }
        return null;
    }

    /**
     * Clears the server list of all items, then re-populates from the list of stored servers. If the current values in
     * the address and username boxes don't match an existing server definition, they will be added to the server list
     * as an unsaved option.
     */
    protected void adjustPanelState() {
        final ItemListener[] listeners = _serverList.getItemListeners();
        for (final ItemListener listener : listeners) {
            _serverList.removeItemListener(listener);
        }
        _serverListModel.refresh();
        for (final ItemListener listener : listeners) {
            _serverList.addItemListener(listener);
        }

        final List<String> servers = _settings.getXnatServerNames();
        Collections.sort(servers);

        if (servers.size() > 0) {
            final XnatServer server = _settings.getCurrentXnatServer();
            if (server != null) {
                _serverList.setSelectedItem(server);
            } else {
                _serverList.setSelectedIndex(0);
                _settings.setCurrentXnatServer(_serverList.getItemAt(0));
            }
            _edit.setEnabled(true);
            _delete.setEnabled(true);
            _serverList.setEnabled(servers.size() > 1);
            _serverList.setVisible(true);

            _password.requestFocusInWindow();
        } else {
            if (_serverList.getItemCount() > 0) {
                _serverList.removeAllItems();
            }
            _parent.showConfigurationPanel();
        }
    }

    private final XnatServerAuthenticationPage _parent;
    private final UploadAssistantSettings      _settings;
    private final JComboBox<XnatServer>        _serverList;
    private final XnatServerComboBoxModel      _serverListModel;
    private final JButton                      _create;
    private final JButton                      _edit;
    private final JButton                      _delete;
    private final JPasswordField               _password;
}
