/*
 * upload-assistant: org.nrg.xnat.upload.ui.XnatServerComboBoxModel
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.net.XnatServer;

import javax.swing.*;
import java.util.Comparator;

public class XnatServerComboBoxModel extends DefaultComboBoxModel<XnatServer> {
    private final UploadAssistantSettings _settings;

    public XnatServerComboBoxModel(final UploadAssistantSettings settings) {
        _settings = settings;
        refresh();
    }

    @Override
    public void addElement(final XnatServer server) {
        insertElementAt(server, 0);
    }

    @Override
    public void insertElementAt(final XnatServer element, int index) {
        int size = getSize();

        //  Determine where to insert element to keep model in sorted order
        for (index = 0; index < size; index++) {
            final XnatServer server = getElementAt(index);
            if (COMPARATOR.compare(server, element) > 0) {
                break;
            }
        }

        super.insertElementAt(element, index);

        //  Select an element when it is added to the beginning of the model
        if (index == 0 && element != null) {
            setSelectedItem(element);
        }
    }

    public void refresh() {
        removeAllElements();
        for (final XnatServer server : _settings.getXnatServers()) {
            addElement(server);
        }
    }

    private static final Comparator<XnatServer> COMPARATOR = new Comparator<XnatServer>() {
        @Override
        public int compare(final XnatServer first, final XnatServer second) {
            if (first == null && second == null) {
                return 0;
            }
            if (first == null) {
                return -1;
            }
            if (second == null) {
                return 1;
            }
            return first.getName().compareTo(second.getName());
        }
    };
}
