/*
 * upload-assistant: org.nrg.xnat.upload.ui.SelectProjectPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.json.JSONException;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;

public final class SelectProjectPage extends WizardPage {
    public static String getDescription() {
        return Messages.getPageTitle(SelectProjectPage.class);
    }

    public SelectProjectPage(final Dimension dimensions) throws IOException, JSONException {
        super();

        // We could just load the JList with Project objects.  Instead, we load it with
        // labels and create the Projects dynamically upon selection, because the Project
        // constructor spins off threads to retrieve the contained subjects and sessions.
        // This is smart if we're creating one Project at a time, but troublesome when
        // creating lots of Projects.
        _log.trace("initializing SelectProjectPage; querying XNAT for project labels");

        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        _projects = new JList<String>() {{
            setName("projects");
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            addMouseListener(new DoubleClickListener(SelectProjectPage.this));
        }};

        _dimensions = dimensions;

        setOpaque(true);
        setLongDescription(Messages.getPageDescription(SelectProjectPage.class));
        setCursor(Cursor.getDefaultCursor());
    }

    /**
     * {@inheritDoc}
     */
    public void renderingPage() {
        _log.trace("rendering SelectProjectPage");
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            final List<String> projects = getCurrentXnatServer().getProjects();
            if (projects.size() > 0) {
                Collections.sort(projects);
                _projects.setListData(new Vector<>(projects));
                _projects.setEnabled(true);
                if (StringUtils.isNotBlank(getCurrentXnatServer().getCurrentProjectId())) {
                    _projects.setSelectedValue(getCurrentXnatServer().getCurrentProjectId(), true);
                } else {
                    _projects.setSelectedIndex(0);
                }
                _log.trace("SelectProjectPage ready with projects {}", Arrays.toString(_projects.getComponents()));
            } else {
                _projects.setListData(new Vector<>(Collections.singletonList(Messages.getMessage("selectprojectpage.noprojects.message", getCurrentXnatServer().getAddress()))));
                _projects.setEnabled(false);
                _log.trace("SelectProjectPage has no projects for server ", getCurrentXnatServer().getAddress());
            }
        } catch (IOException e) {
            _log.error("An error occurred trying to retrieve the list of projects from the current server at " + getCurrentXnatServer().getAddress(), e);
        }
        final JScrollPane pane = new JScrollPane(_projects);
        if (null != _dimensions) {
            pane.setPreferredSize(_dimensions);
        }
        add(pane);
        stopWatch.stop();
        _log.info("It took {}ms to get the projects for the server and populate the list.", stopWatch.getTime());
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _projects.requestFocusInWindow();
            }
        });

    }

    /**
     * {@inheritDoc}
     */
    protected void recycle() {
        _log.info("Recycling SelectSubjectPage");
        _projects.setModel(new DefaultListModel<String>());
        removeAll();
        validate();
    }

    /**
     * Indicates whether the state of the page is valid.
     *
     * @param component The component to be validated.
     * @param object    The object, usually an event.
     *
     * @return Returns null if the page state is valid, otherwise returns a message indicating what needs to be done.
     */
    protected String validateContents(final Component component, final Object object) {
        if (object != null && ((ListSelectionEvent) object).getValueIsAdjusting()) {
            return _validation;
        }
        return _validation = _projects.getSelectedIndex() < 0 ? "" : null;
    }

    /**
     * This always allows the user to go back, but removes the project object set in the settings object.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.PROCEED</b> in all cases.
     */
    public WizardPanelNavResult allowBack(String stepName, Map settings, Wizard wizard) {
        getCurrentXnatServer().setCurrentProjectId(null);
        return WizardPanelNavResult.PROCEED;
    }

    /**
     * Indicates whether the user can proceed to the next page. For this page, the current directory in the file chooser
     * is set as the current directory in preferences to be reused for future operations. There are no checks that block
     * the user's ability to proceed.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.PROCEED</b> in all cases.
     */
    @Override
    public WizardPanelNavResult allowNext(final String stepName, final Map settings, final Wizard wizard) {
        final String projectId = _projects.getSelectedValue();
        if (StringUtils.isNotBlank(projectId)) {
            getCurrentXnatServer().setCurrentProjectId(projectId);
            return WizardPanelNavResult.PROCEED;
        }
        return WizardPanelNavResult.REMAIN_ON_PAGE;
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        final Project project = getCurrentXnatServer().getCurrentProject();
        return project != null ? project.toString() : "";
    }

    private final Logger _log = LoggerFactory.getLogger(SelectProjectPage.class);

    private final JList<String> _projects;
    private final Dimension     _dimensions;

    private String _validation = "";
}
