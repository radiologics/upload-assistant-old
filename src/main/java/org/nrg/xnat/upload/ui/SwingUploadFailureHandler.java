/*
 * upload-assistant: org.nrg.xnat.upload.ui.SwingUploadFailureHandler
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import java.awt.Component;
import java.util.Collection;
import java.util.Map;

import javax.swing.JOptionPane;

import org.nrg.xnat.upload.data.UploadFailureHandler;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public class SwingUploadFailureHandler implements UploadFailureHandler {
    private static final int                   DEFAULT_MAX_TRIES     = 3;
    private static final Object[]              DIALOG_OPTIONS        = new Object[]{
            Messages.getMessage("error.uploadfailure.tryagain"),
            Messages.getMessage("error.uploadfailure.stop")
    };
    private static final Object                DEFAULT_DIALOG_OPTION = DIALOG_OPTIONS[0];
    private static final Map<Integer, Boolean> actions               = ImmutableMap.of(
            JOptionPane.YES_OPTION, true,
            JOptionPane.NO_OPTION, false
                                                                                      );
    private final Logger logger = LoggerFactory.getLogger(SwingUploadFailureHandler.class);
    private final Component parent;
    private final int       maxTries;
    private final Multimap<Object, Object>                   failures   = LinkedListMultimap.create();
    // Keep one instance of each exception class
    private final Map<Class<? extends Throwable>, Throwable> throwables = Maps.newLinkedHashMap();

    SwingUploadFailureHandler(final Component parent, final int maxTries) {
        this.parent = parent;
        this.maxTries = maxTries;
    }

    SwingUploadFailureHandler() {
        this(null, DEFAULT_MAX_TRIES);
    }

    public boolean shouldRetry(final Object item, final Object cause) {
        if (failures.size() < maxTries) {
            insertFailure(item, cause);
            return true;
        } else {
            insertFailure(item, cause);
            return actions.get(JOptionPane.showOptionDialog(parent,
                                                            Messages.getDialogFormattedMessage("error.uploadfailure.message", item, cause, failures.size(), getFailuresDescription(failures)),
                                                            Messages.getMessage("error.uploadfailure.title"),
                                                            JOptionPane.YES_NO_OPTION,
                                                            JOptionPane.ERROR_MESSAGE,
                                                            null,
                                                            DIALOG_OPTIONS,
                                                            DEFAULT_DIALOG_OPTION));
        }
    }

    private void insertFailure(final Object item, final Object cause) {
        if (cause instanceof Throwable) {
            final Throwable t = (Throwable) cause;
            final Class<? extends Throwable> tclass = t.getClass();
            failures.put(tclass, item);
            //noinspection ThrowableResultOfMethodCallIgnored
            throwables.put(tclass, t);
            logger.debug("unable to upload " + item, t);
        } else {
            logger.debug("unable to upload {}: {}", item, cause);
            failures.put(cause, item);
        }
    }

    private String getFailuresDescription(final Multimap<Object, ?> failures) {
        final StringBuilder buffer = new StringBuilder();

        for (final Object cause : failures.keySet()) {
            final StringBuilder failure = new StringBuilder();
            if (cause instanceof Class<?>) {
                @SuppressWarnings("unchecked")
                final Class<? extends Throwable> clazz = (Class<? extends Throwable>) cause;
                failure.append(clazz.getSimpleName());
                @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
                final Throwable instance = throwables.get(clazz);
                failure.append(" (").append(instance.getLocalizedMessage()).append(")");
            } else {
                failure.append(cause);
            }
            failure.append(": ");
            final Collection<?> objects = failures.get(cause);
            switch (objects.size()) {
                case 1:
                    failure.append(objects.iterator().next());
                    break;

                // TODO: enumerate small numbers?
                default:
                    failure.append(Messages.getMessage("error.uploadfailure.items", objects.size()));
                    failure.append(objects.iterator().next());
                    break;
            }
            buffer.append(Messages.getMessage("error.uploadfailure.failure", failure.toString()));
        }
        return Messages.getMessage("error.uploadfailure.failures", buffer.toString());
    }

    public final String getButtonTextFor(final boolean shouldRetry) {
        return DIALOG_OPTIONS[shouldRetry ? 0 : 1].toString();
    }
}
