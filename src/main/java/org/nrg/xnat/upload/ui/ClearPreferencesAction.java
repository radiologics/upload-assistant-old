/*
 * upload-assistant: org.nrg.xnat.upload.ui.ClearPreferencesAction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.prefs.BackingStoreException;

public class ClearPreferencesAction extends AbstractAction {
    public static final String APPLICATION_SETTINGS = "applicationSettings";

    public ClearPreferencesAction(final Component component, final UploadAssistantSettings settings) {
        super(Messages.getMessage("menu.clear.title"));
        _component = component;
        _settings = settings;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        _log.info("Now performing the action {}", event.getActionCommand());
        final int confirm = JOptionPane.showOptionDialog(_component,
                                                         Messages.getDialogFormattedMessage("menu.clear.message"),
                                                         (String) getValue(Action.NAME), JOptionPane.YES_NO_OPTION,
                                                         JOptionPane.QUESTION_MESSAGE, null, null, null);
        if (confirm == JOptionPane.YES_OPTION) {
            try {
                _settings.clearSavedSettings();
                firePropertyChange(APPLICATION_SETTINGS, "old", "cleared");
            } catch (BackingStoreException e) {
                JOptionPane.showMessageDialog(_component, Messages.getDialogFormattedMessage("menu.clear.error.message", e.getMessage()),
                                              Messages.getMessage("menu.clear.error.title"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private static final Logger _log = LoggerFactory.getLogger(ClearPreferencesAction.class);

    private final Component                    _component;
    private final UploadAssistantSettings      _settings;
}
