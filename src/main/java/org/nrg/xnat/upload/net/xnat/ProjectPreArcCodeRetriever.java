/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.ProjectPreArcCodeRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net.xnat;

import org.nrg.framework.constants.PrearchiveCode;
import org.nrg.xnat.upload.net.StringResponseProcessor;

import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public class ProjectPreArcCodeRetriever implements Callable<PrearchiveCode> {
    public ProjectPreArcCodeRetriever(final String project) {
        _path = String.format(URL_TEMPLATE, project);
    }

    @Override
    public PrearchiveCode call() throws Exception {
        final StringResponseProcessor processor = new StringResponseProcessor();
        getRestServer().doGet(_path, processor);
        return PrearchiveCode.code(processor.toString());
    }

    private static final String URL_TEMPLATE = "/data/archive/projects/%s/prearchive_code";

    private final String _path;
}
