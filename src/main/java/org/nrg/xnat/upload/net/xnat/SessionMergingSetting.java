package org.nrg.xnat.upload.net.xnat;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.StringResponseProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class SessionMergingSetting implements Callable<List<String>> {
    public List<String> call() {
        final String version = getCurrentXnatServer().getServerVersion();
        if (StringUtils.startsWith("1.7", version)) {
            return getSessionMergingSetting(SITE_17_PATH);
        } else if (StringUtils.startsWith("1.6", version)) {
            return getSessionMergingSetting(SITE_16_PATH);
        }
        _log.warn("Didn't get an understandable version back from the server. Here it is for what it's worth: {}", version);
        return Collections.emptyList();
    }

    private List<String> getSessionMergingSetting(final String path) {
        final StringResponseProcessor processor = new StringResponseProcessor();
        try {
            getRestServer().doGet(path, processor);
            final String response = processor.toString();
            if (StringUtils.isBlank(response)) {
                return Collections.emptyList();
            }
            final JSONArray json = new JSONArray(response);
            return Lists.transform(json.toList(), new Function<Object, String>() {
                @Nullable
                @Override
                public String apply(@Nullable final Object object) {
                    if (object == null) {
                        return null;
                    }
                    return (String) object;
                }
            });
        } catch (HttpException e) {
            // Check the status.
            final int status = e.getResponseCode();
            // 400 or 404 usually indicates 1.6 without setting, that's fine.
            if (!((status == 400 || status == 404) && path.equals(SITE_16_PATH))) {
                _log.warn("Got a weird response from the server: " + e.getResponseCode() + " " + e.getMessage());
            }
        } catch (Exception e) {
            _log.error("Got a bad error of some sort", e);
        }
        return Collections.emptyList();
    }

    private static final String SITE_16_PATH = "/data/services/settings/security.fail_merge_on";
    private static final String SITE_17_PATH = "/xapi/siteConfig/failMergeOn";

    private static final Logger _log = LoggerFactory.getLogger(SessionMergingSetting.class);
}
