package org.nrg.xnat.upload.net.xnat;

import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.upload.net.StringResponseProcessor;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class RequireDateSettingRetriever implements Callable<Boolean> {
    public RequireDateSettingRetriever(final String project) {
        _projectPath = String.format(URL_TEMPLATE_PROJECT, project);
    }

    public Boolean call() {
        final StringResponseProcessor processor = new StringResponseProcessor();
        try {
            // check to see if we got back a status page instead of the setting
            // if so, there's no project specific list, so just get the site wide setting
            try {
                getRestServer().doGet(_projectPath, processor);
            } catch (Throwable t) {
                getRestServer().doGet(URL_TEMPLATE_SITE, processor);
            }
            String setting = processor.toString();

            return (StringUtils.equalsIgnoreCase("true", setting) || StringUtils.equalsIgnoreCase("true\n", setting));
        } catch (Throwable t) {
            //default to not required
            return false;
        }
    }

    private static final String URL_TEMPLATE_SITE    = "/data/config/applet/require-date?contents=true";
    private static final String URL_TEMPLATE_PROJECT = "/data/projects/%s/config/applet/require-date?contents=true";

    private final String _projectPath;
}
