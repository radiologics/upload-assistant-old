/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.PETTracerRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import com.google.common.collect.Sets;
import org.nrg.xnat.upload.net.StringListConnectionProcessor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class PETTracerRetriever implements Callable<Set<String>> {
    public PETTracerRetriever(final String project) {
        _projectPath = String.format(URL_TEMPLATE_PROJECT, project);
    }

    public Set<String> call() {
        final StringListConnectionProcessor processor = new StringListConnectionProcessor();
        try {
            // check to see if we got back a status page instead of a list of tracers
            // if so, there's no project specific list, so just get the site list instead
            try {
                getRestServer().doGet(_projectPath, processor);
            } catch (Throwable t) {
                getRestServer().doGet(URL_TEMPLATE_SITE, processor);
            }
            return Sets.newLinkedHashSet(processor);
        } catch (Throwable t) {
            return getDefaultTracers();
        }
    }

    public static Set<String> getDefaultTracers() {
        return Sets.newLinkedHashSet(DEFAULT_TRACERS);
    }

    private static Set<String> getDefaultTracers(final String resource) {
        try (final InputStream input = PETTracerRetriever.class.getResourceAsStream(resource)) {
            if (null == input) {
                throw new RuntimeException("Unable to load default PET tracers");
            }
            return Sets.newLinkedHashSet(StringListConnectionProcessor.readStrings(input));
        } catch (IOException e) {
            throw new RuntimeException("Unable to read default PET tracers", e);
        }
    }

    private static final String      URL_TEMPLATE_SITE        = "/data/config/tracers/tracers?contents=true";
    private static final String      URL_TEMPLATE_PROJECT     = "/data/projects/%s/config/tracers/tracers?contents=true";
    private static final String      DEFAULT_TRACERS_RESOURCE = "/PET-tracers.txt";
    private static final Set<String> DEFAULT_TRACERS          = getDefaultTracers(DEFAULT_TRACERS_RESOURCE);

    private final String _projectPath;
}
