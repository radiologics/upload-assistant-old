/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.BuildInfoRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net.xnat;

import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.StringResponseProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class BuildInfoRetriever implements Callable<Map<String, String>> {
    public Map<String, String> call() {
        final Map<String, String> buildInfo = getXnat17BuildInfo();
        return buildInfo != null ? buildInfo : getXnat16BuildInfo();
    }

    private Map<String, String> getXnat17BuildInfo() {
        try {
            return getRestServer().getConfigurationProperties(BUILD_INFO_17_PATH);
        } catch (IOException e) {
            handleException(e);
        }
        return null;
    }

    private Map<String, String> getXnat16BuildInfo() {
        final StringResponseProcessor processor = new StringResponseProcessor();
        try {
            getRestServer().doGet(BUILD_INFO_16_PATH, processor);
            return new HashMap<String, String>() {{
                put("version", processor.toString());
            }};
        } catch (Exception e) {
            handleException(e);
        }
        return null;
    }

    private void handleException(final Exception e) {
        // HTTP 404 usually indicates 1.6, so no need to log anything.
        if (e instanceof HttpException) {
            final int status = ((HttpException) e).getResponseCode();
            if (status != 404) {
                _log.warn("An HTTP error occurred trying to retrieve the server build info. Status " + status + ": " + e.getMessage());
            }
        } else {
            _log.warn("An error occurred trying to retrieve the server build info: " + e.getMessage());
        }
    }

    private static final String BUILD_INFO_17_PATH = "/xapi/siteConfig/buildInfo";
    private static final String BUILD_INFO_16_PATH = "/data/version";
    private static final Logger _log               = LoggerFactory.getLogger(BuildInfoRetriever.class);
}
