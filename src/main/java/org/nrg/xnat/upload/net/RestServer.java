/*
 * upload-assistant: org.nrg.xnat.upload.net.RestServer
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.IOUtils;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.ui.Constants;
import org.nrg.xnat.upload.ui.UIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.net.HttpURLConnection.*;
import static org.nrg.dicom.mizer.service.http.HttpClient.getBase64Encoded;
import static org.nrg.xnat.upload.net.XnatConnectionException.Failure.InvalidAddress;
import static org.nrg.xnat.upload.net.XnatConnectionException.Failure.UnverifiedHttps;

public class RestServer {

    public RestServer(final UploadAssistantSettings settings) {
        _settings = settings;
        _defaultHeaders = ImmutableMap.of("User-Agent", StringUtils.remove(_settings.getProperty(Constants.APPLICATION_NAME), " ") + "/" + _settings.getProperty(Constants.APPLICATION_VERSION), "Accept", "*/*");
    }

    public static boolean isUnverifiedConnection(final HttpURLConnection connection) {
        return connection instanceof HttpsURLConnection && ((HttpsURLConnection) connection).getHostnameVerifier().equals(TRUSTING_VERIFIER);
    }

    public Map<String, String> getConfigurationProperties(final String path, String... properties) throws IOException {
        try {
            final JSONConfigurationExtractor extractor = new JSONConfigurationExtractor();
            doGet(path, extractor);
            final Map<String, String> configuration = new HashMap<>(properties.length);
            for (final String property : properties) {
                if (extractor.containsKey(property)) {
                    configuration.put(property, extractor.get(property).toString());
                } else {
                    configuration.put(property, "");
                }
            }
            return configuration;
        } catch (HttpException e) {
            if (e.getResponseCode() == 404) {
                return null;
            }
            throw e;
        }
    }

    public LinkedHashMap<String, String> getSeriesImportFilter(final String path) throws IOException, JSONException {
        final String query;
        if (!path.contains("format=json")) {
            try {
                URI uri = new URI(path);
                if (StringUtils.isBlank(uri.getQuery())) {
                    query = "?format=json";
                } else {
                    query = "&format=json";
                }
            } catch (URISyntaxException ignored) {
                // Not going to work about this because it's internal use.
                throw new IOException("There was an error processing the series import filter path: " + path, ignored);
            }
        } else {
            query = "";
        }

        final JSONConfigurationExtractor extractor = new JSONConfigurationExtractor();
        doGet(path + query, extractor);
        final LinkedHashMap<String, String> filter = new LinkedHashMap<>();
        for (final String key : extractor.keySet()) {
            final Object value = extractor.get(key);
            filter.put(key, convertObjectToJSON(value));
        }
        return filter;
    }

    public void doGet(final String path, final HttpURLConnectionProcessor processor)
            throws Exception {
        request(path, GET, processor);
    }

    public void doGet(final String path, final JSONDecoder decoder)
            throws IOException, JSONException {
        try {
            doGet(path, new JSONResultExtractor(decoder));
        } catch (IOException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void doPost(final String path, final HttpURLConnectionProcessor processor)
            throws Exception {
        request(path, POST, processor);
    }

    @SuppressWarnings("unused")
    public void doPut(final String path, final HttpURLConnectionProcessor processor) throws Exception {
        request(path, PUT, processor);
    }

    /**
     * Performs a POST request with empty entity.
     *
     * @param path Path to POST resource.
     *
     * @throws IOException When error occurs during server I/O.
     */
    @SuppressWarnings("unused")
    public void doPost(final String path) throws IOException {
        request(path, POST);
    }

    /**
     * Performs a PUT request with empty entity.
     *
     * @param path Path to PUT resource.
     *
     * @throws IOException When error occurs during server I/O.
     */
    @SuppressWarnings("unused")
    public void doPut(final String path) throws IOException {
        request(path, PUT);
    }

    @SuppressWarnings("unused")
    public void doPut(final String path, final File f, final String mimeMediaType, final ResultProgressHandle progress) throws IOException {
        request(path, PUT, f, mimeMediaType, progress);
    }

    public Collection<Object> getValues(final String path, final String key) throws IOException, JSONException {
        final JSONValuesExtractor extractor = new JSONValuesExtractor(new LinkedHashSet<>(), key);
        doGet(path, extractor);
        return extractor.getValues();
    }

    public Map<String, String> getValueMap(final String path, final String... keys) throws IOException, JSONException {
        final JSONValueMapExtractor extractor = new JSONValueMapExtractor(keys);
        doGet(path, extractor);
        return extractor.getValues();
    }

    public Map<String, String> getAliases(final String path) throws IOException, JSONException {
        final JSONAliasesExtractor extractor = new JSONAliasesExtractor(new LinkedHashMap<String, String>(), JSON_ALIAS_KEY, JSON_ID_KEY);
        doGet(path, extractor);
        return extractor.getAliases();
    }

    public void updateSessionCookies(final HttpURLConnection connection) {
        _settings.getCurrentXnatServer().setSessionCookies(connection);
    }

    public void verifyServer(final XnatServer server) throws XnatConnectionException {
        if (server == null || StringUtils.isBlank(server.getAddress())) {
            throw new XnatConnectionException(XnatConnectionException.Failure.EmptyAddress);
        }

        try {
            final HttpURLConnection connection = getConnectionWithProtocolFailover(server);
            validateConnectionStatus(connection);
            if (connection != null) {
                server.verify(connection);
            }
        } catch (MalformedURLException e) {
            throw new XnatConnectionException(InvalidAddress, server.getAddress());
        } catch (IOException e) {
            throw new XnatConnectionException(XnatConnectionException.Failure.Unknown, server.getAddress(), e);
        }
    }

    private void validateConnectionStatus(final HttpURLConnection connection) throws IOException, XnatConnectionException {
        final int response = connection.getResponseCode();
        final URL url      = connection.getURL();
        switch (response) {
            case 200:
                logger.debug("Got response 200 authenticating against {}", url.toString());
                return;

            case 401:
                throw new XnatConnectionException(XnatConnectionException.Failure.AuthenticationFailed, url);

            case 403:
                throw new XnatConnectionException(XnatConnectionException.Failure.Unauthorized, url);

            case 302:
            case 404:
                // The 302 is an odd case: some sites (Yahoo) will respond to the auth call with a 302. There's
                // no case in which an XNAT server with a valid address should do this.
                throw new XnatConnectionException(XnatConnectionException.Failure.NotAnXnatServer, url);

            case 500:
                throw new XnatConnectionException(XnatConnectionException.Failure.SystemError, url);

            default:
                throw new XnatConnectionException(XnatConnectionException.Failure.Unknown, url);
        }
    }

    private HttpURLConnection getConnectionWithProtocolFailover(final XnatServer server) throws MalformedURLException, XnatConnectionException, ConnectException {
        final String address = server.getAddress();
        if (StringUtils.isBlank(address)) {
            return null;
        }

        final PasswordAuthentication authentication       = server.getPasswordAuthentication();
        final Boolean                allowUnverifiedHttps = server.isAllowUnverifiedHttps();
        final URL                    url                  = UIUtils.getVerifyXnatUrl(address);

        try {
            // This just attempts to see if the system knows how to resolve the host address to an IP. If not, it's
            // unreachable for sure.
            InetAddress.getByName(url.getHost());
        } catch (UnknownHostException e) {
            throw new XnatConnectionException(XnatConnectionException.Failure.InvalidAddress, address);
        }

        try {
            return getConnectionWithUnverifiedFailover(url, authentication, allowUnverifiedHttps);
        } catch (ConnectException e) {
            return !UIUtils.hasProtocol(address) ? getConnectionWithUnverifiedFailover(UIUtils.getVerifyXnatUrl("http://" + address), authentication, allowUnverifiedHttps) : null;
        }
    }

    private HttpURLConnection getConnectionWithUnverifiedFailover(final URL url, final PasswordAuthentication authentication, final Boolean allowUnverifiedHttps) throws XnatConnectionException, ConnectException {
        try {
            try {
                final HttpURLConnection connection = getNewConnection(url, "GET", false, getConnectionHeaders(authentication));
                connection.getResponseCode();
                return connection;
            } catch (SSLHandshakeException e) {
                if (allowUnverifiedHttps) {
                    logger.debug("Got SSLHandshakeException authenticating against {} as an unverified HTTPS connection with username {}, trying with unverified", url.toString(), authentication.getUserName());
                    final HttpURLConnection connection = getNewConnection(url, "GET", true, getConnectionHeaders(authentication));
                    connection.getResponseCode();
                    return connection;
                } else {
                    throw new XnatConnectionException(UnverifiedHttps, url);
                }
            } catch (ProtocolException e) {
                throw new XnatConnectionException(InvalidAddress, url, e);
            }
        } catch (IOException e) {
            if (e instanceof ConnectException) {
                throw (ConnectException) e;
            }
            throw new XnatConnectionException(XnatConnectionException.Failure.Unknown, url, e);
        }
    }

    @SuppressWarnings("SameParameterValue")
    public HttpURLConnection getNewConnection(final URL url, final String method, final XnatServer server) throws IOException {
        final Boolean             unverifiedHttps = server.getUsingUnverifiedHttps();
        final Map<String, String> headers         = getConnectionHeaders(server.getPasswordAuthentication());
        return getNewConnection(url, method, unverifiedHttps, headers);
    }

    private HttpURLConnection getNewConnection(final URL url, final String method, final Boolean unverifiedHttps, final Map<String, String> headers) throws IOException {
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        setConnectionHeaders(connection, headers);
        if (unverifiedHttps != null && unverifiedHttps && connection instanceof HttpsURLConnection) {
            ((HttpsURLConnection) connection).setHostnameVerifier(TRUSTING_VERIFIER);
            ((HttpsURLConnection) connection).setSSLSocketFactory(TRUSTING_SOCKET_FACTORY);
        }
        return connection;
    }

    static JSONObject extractJSONEntity(final InputStream in)
            throws IOException, JSONException {
        return new JSONObject(new JSONTokener(new InputStreamReader(in)));
    }

    static JSONArray extractResultFromEntity(final JSONObject entity)
            throws JSONException {
        try {
            return entity.getJSONObject("ResultSet").getJSONArray("Result");
        } catch (JSONException e) {
            final String message = e.getMessage();
            if (message.contains("JSONObject[\"ResultSet\"] not found.")) {
                final JSONArray array = new JSONArray();
                array.put(entity);
                return array;
            }
            throw e;
        }
    }

    /**
     * Creates the basic authentication header and adds it to the submitted header map.
     *
     * @param headers        A map of existing headers for a connection.
     * @param authentication A populated authentication object.
     */
    private static void addBasicAuthorizationToHeaderMap(final Map<String, String> headers, final PasswordAuthentication authentication) {
        headers.put(AUTHORIZATION_HEADER, getBase64Encoded(authentication));
    }

    /**
     * This method converts a value of an unknown type into a valid JSON string. This works around issues where
     * calling the <b>toString()</b> method on, e.g., a map object resulted in invalid JSON.
     *
     * @param value The object to be converted to JSON.
     *
     * @return A string representing a valid JSON serialization for the submitted object.
     */
    private static String convertObjectToJSON(final Object value) {
        if (value == null) {
            return "";
        }
        if (value instanceof String) {
            return (String) value;
        }
        if (value instanceof Map) {
            final Map        map        = (Map) value;
            final JSONObject jsonObject = new JSONObject();
            for (final Object key : map.keySet()) {
                jsonObject.put(key.toString(), map.get(key));
            }
            return jsonObject.toString();
        }
        if (value instanceof List) {
            return new JSONArray((List) value).toString();
        }
        // This tells you if you have an array of objects as opposed to an array of primitives
        // (which are a pain to work with and probably shouldn't even happen in this context,
        // so we're going to ignore them.
        if (value instanceof Object[] && value.getClass().isArray()) {
            return new JSONArray(Arrays.asList((Object[]) value)).toString();
        }
        // At this point, we have no idea what kind of nonsense someone threw over the fence
        // to us, so just give it back to them as a string.
        return value.toString();
    }

    private void request(final String path, final String method, final HttpURLConnectionProcessor processor) throws Exception {
        final XnatServer    server = _settings.getCurrentXnatServer();
        final StringBuilder sb     = new StringBuilder(server.getValidatedUrl().toString());
        if ('/' != path.charAt(0)) {
            sb.append('/');
        }
        sb.append(path);
        final URL url = new URL(sb.toString());
        logger.trace("{} preparing request {}", this, url);
        if (logger.isTraceEnabled()) {
            final Callable<List<String>> callable = new Callable<List<String>>() {
                public List<String> call() throws IOException, URISyntaxException {
                    final CookieHandler             ch = CookieHandler.getDefault();
                    final Map<String, List<String>> h  = ch.get(url.toURI(), new HashMap<String, List<String>>());
                    return h.get("Cookie");
                }
            };
            final ExecutorService es = Executors.newSingleThreadExecutor();
            try {
                final List<String> cookies = es.invokeAny(Collections.singleton(callable), 10, TimeUnit.SECONDS);
                logger.trace("session cookies: {}", cookies);
            } catch (Exception e) {
                logger.error("Unable to query cookie store", e);
            }
        }

        final Map<String, String> headers = getConnectionHeaders();

        logger.trace("opening connection to {}", url);

        HttpURLConnection connection = null;
        int               attempts   = 0;
        TRY_PUT:
        for (; ; ) {
            try {
                connection = getNewConnection(url, method, server.getUsingUnverifiedHttps(), headers);
                processor.prepare(connection);
                final int responseCode = connection.getResponseCode();
                updateSessionCookies(connection);

                switch (responseCode) {
                    case HTTP_ACCEPTED:
                    case HTTP_NOT_AUTHORITATIVE:
                    case HTTP_NO_CONTENT:
                    case HTTP_RESET:
                    case HTTP_PARTIAL:
                    case HTTP_MOVED_PERM:
                        logger.trace(connection.getRequestMethod() + " to {} returned "
                                     + responseCode + " ({})",
                                     url, connection.getResponseMessage());

                    case HTTP_OK:
                    case HTTP_CREATED:
                        processor.process(connection);
                        return;

                    // Handle 302, at least temporarily: Spring auth redirects to login page,
                    // so assume that's what's happened when we see a redirect at this point.
                    case HTTP_MOVED_TEMP:
                    case HTTP_UNAUTHORIZED:
                        if (logger.isDebugEnabled()) {
                            logger.debug("Received status code " + (responseCode == HTTP_MOVED_TEMP ? "302 (Redirect)" : "401 (Unauthorized)"));
                            for (final Map.Entry<String, List<String>> me : connection.getHeaderFields().entrySet()) {
                                logger.trace("Header {} : {}", me.getKey(), me.getValue());
                            }
                            logger.debug("Will request credentials for {}", url);
                        }
                        addBasicAuthorizationToHeaderMap(headers, server.getPasswordAuthentication());

                        if (attempts++ < 3) {
                            continue TRY_PUT;
                        }
                        throw new HttpException(responseCode, connection.getResponseMessage(), connection.getRequestMethod() + " " + url);

                    case HTTP_NOT_FOUND:
                        throw new NotFoundHttpException(connection.getRequestMethod() + " " + url);

                    case HTTP_CONFLICT:
                        throw new ConflictHttpException(getErrorEntity(connection));

                    default:
                        throw new HttpException(responseCode, connection.getResponseMessage(),
                                                connection.getRequestMethod() + " " + url,
                                                getErrorEntity(connection));
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }
    }

    private Map<String, String> getConnectionHeaders() {
        return getConnectionHeaders(null);
    }

    private Map<String, String> getConnectionHeaders(final PasswordAuthentication authentication) {
        final XnatServer          server  = _settings.getCurrentXnatServer();
        final Map<String, String> headers = Maps.newLinkedHashMap(_defaultHeaders);
        if (authentication != null) {
            addBasicAuthorizationToHeaderMap(headers, authentication);
            if (server != null) {
                if (StringUtils.equals(server.getUsername(), authentication.getUserName())) {
                    server.setPassword(authentication.getPassword());
                }
            }
        } else if (server != null) {
            if (server.hasSessionId()) {
                headers.put("Cookie", Joiner.on("; ").join(server.getSessionCookies()));
            } else if (server.getPasswordAuthentication() != null) {
                addBasicAuthorizationToHeaderMap(headers, server.getPasswordAuthentication());
            }
        }
        return headers;
    }

    private static void setConnectionHeaders(final HttpURLConnection connection, final Map<String, String> headers) {
        for (final Map.Entry<String, String> header : headers.entrySet()) {
            if (!connection.getRequestProperties().containsKey(header.getKey())) {
                connection.setRequestProperty(header.getKey(), header.getValue());
            }
        }
    }

    private static String getErrorEntity(final HttpURLConnection connection) throws IOException {
        final InputStream errorStream = connection.getErrorStream();
        try {
            if (null != errorStream) {
                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                IOUtils.copy(stream, errorStream);
                if (stream.size() > 0) {
                    return stream.toString();
                }
            }
            return null;
        } finally {
            if (errorStream != null) {
                try {
                    errorStream.close();
                } catch (IOException ignored) {
                    // Just ignore this if it happens. This will allow exceptions from the top through and just not say
                    // anything if the close fails. Java gets unhappy about throwing exceptions from finally blocks.
                }
            }
        }
    }

    private static SSLSocketFactory getTrustingSocketFactory() {
        try {
            final SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, TRUSTING_MANAGER, new SecureRandom());
            return context.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            logger.error("An error occurred creating a trusting socket factory. Support for unverified connections may not function properly.", e);
            return null;
        }
    }

    private void request(final String path, final String method) throws IOException {
        try {
            request(path, method, new EmptyRequest());
        } catch (IOException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void request(final String path, final String method,
                         final InputStream in, final String mimeMediaType, final Integer contentLength,
                         final ResultProgressHandle progress)
            throws IOException {
        try {
            request(path, method, new StreamUploadProcessor(in, mimeMediaType, contentLength, progress));
        } catch (IOException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void request(final String path, final String method,
                         final File f, final String mimeMediaType, final ResultProgressHandle progress)
            throws IOException {
        request(path, method, new FileInputStream(f),
                mimeMediaType, Long.valueOf(f.length()).intValue(), progress);
    }

    private static final TrustManager[] TRUSTING_MANAGER = new TrustManager[]{new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(final X509Certificate[] certs, final String authType) {
        }

        public void checkServerTrusted(final X509Certificate[] certs, final String authType) {
        }
    }};

    // Create all-trusting host name verifier
    private static final HostnameVerifier TRUSTING_VERIFIER = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static final SSLSocketFactory TRUSTING_SOCKET_FACTORY = getTrustingSocketFactory();

    private static final Logger logger = LoggerFactory.getLogger(RestServer.class);

    private static final String GET = "GET", PUT = "PUT", POST = "POST";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String JSON_ALIAS_KEY       = "label";
    private static final String JSON_ID_KEY          = "ID";

    private final UploadAssistantSettings _settings;
    private final Map<String, String>     _defaultHeaders;
}
