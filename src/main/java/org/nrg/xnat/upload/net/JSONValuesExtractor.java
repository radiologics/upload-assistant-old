package org.nrg.xnat.upload.net;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * Extracts values associated with the specified key from JSON objects.
 */
final class JSONValuesExtractor implements JSONDecoder {
    JSONValuesExtractor(final Collection<Object> values, final String key) {
        _values = values;
        _key = key;
    }

    /**
     * Extracts the value corresponding to the configured key from the submitted JSON object.
     *
     * @param jsonObject The object from which to extract values.
     *
     * @throws JSONException When there's an error in the JSON.
     */
    public void decode(final JSONObject jsonObject) throws JSONException {
        _log.trace("decoding {} from {}", _key, jsonObject);
        if (jsonObject.has(_key)) {
            final Object value = jsonObject.get(_key);
            _values.add(value);
            _log.debug("Found value {} for key {}, now have {} values total.", value == null ? "<NULL>" : value.toString(), _key, _values.size());
        }
    }

    /**
     * Returns all the values extracted for the configured key.
     *
     * @return The values extracted for the configured key.
     */
    public Collection<Object> getValues() {
        return _values;
    }

    private static final Logger _log = LoggerFactory.getLogger(JSONValuesExtractor.class);

    private final Collection<Object> _values;
    private final String             _key;
}
