package org.nrg.xnat.upload.util;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.net.xnat.ProjectSession;
import org.slf4j.LoggerFactory;

import java.util.List;

public class VisitTemplate implements VisitTemplateI {
	private final String key;
	private List<VisitOccurrenceI> visits =Lists.newArrayList();
	private final Project project;
	
	
	public VisitTemplate(JSONObject json, Project project) throws JSONException{
		this.key=json.getString("key");
		this.project=project;
	}
	
	public void addVisit(VisitOccurrenceI v){
		this.visits.add(v);
	}

	/* (non-Javadoc)
	 * @see org.nrg.util.VisitTemplateI#getVisits()
	 */
	@Override
	public List<VisitOccurrenceI> getVisits() {
		return visits;
	}

	public void setVisits(List<VisitOccurrenceI> visits) {
		this.visits = visits;
	}

	public void setVisits(JSONArray visits2) {
		for(int i=0;i<visits2.length();i++){
			JSONObject mod= visits2.getJSONObject(i);
			addVisit(new VisitOccurrence(mod, project));
		}
	}

	/* (non-Javadoc)
	 * @see org.nrg.util.VisitTemplateI#getKey()
	 */
	@Override
	public String getKey() {
		return key;
	}

	public VisitTemplate(String key,Project project){
		this.key=key;
		this.project=project;
	}
		
	public static class VisitOccurrence implements VisitOccurrenceI {
		private final String key;
		private final String name;
		private final String code;
		private List<VisitModalityI> modalities =Lists.newArrayList();
		private Subject              subject    =null;
		
		public VisitOccurrence(String key, String name, String code){
			this.name=name;
			this.code=code;
			this.key=key;
		}
		
		public VisitOccurrence(JSONObject json, Project project) throws JSONException{
			this(json.getString("key"),json.getString("name"),json.getString("code"));
			
			JSONArray modalities=json.getJSONArray("modalities");
			for(int i=0;i<modalities.length();i++){
				JSONObject mod= modalities.getJSONObject(i);
				addModality(new VisitModality(mod, this, project));
			}
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitOccurrenceI#getModalities()
		 */
		@Override
		public List<VisitModalityI> getModalities() {
			return modalities;
		}

		public void setModalities(List<VisitModalityI> modalities) {
			this.modalities = modalities;
		}
		
		public void addModality(VisitModalityI modality){
			this.modalities.add(modality);
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitOccurrenceI#getKey()
		 */
		@Override
		public String getKey() {
			return key;
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitOccurrenceI#getName()
		 */
		@Override
		public String getName() {
			return name;
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitOccurrenceI#getCode()
		 */
		@Override
		public String getCode() {
			return code;
		}
		
		/*
	     * (non-Javadoc)
	     * @see java.lang.Object#equals(java.lang.Object)
	     */
	    public boolean equals(final Object o) {
	        return o instanceof VisitOccurrence
	                && Objects.equal(key, ((VisitOccurrence) o).getKey())
	                && Objects.equal(name, ((VisitOccurrence) o).getName())
	                && Objects.equal(code, ((VisitOccurrence) o).getCode());
	    }

	    /*
	     * (non-Javadoc)
	     * @see java.lang.Object#hashCode()
	     */
	    public int hashCode() {
	        return Objects.hashCode(key, name, code);
	    }
		
		/*
	     * (non-Javadoc)
	     * @see java.lang.Object#toString()
	     */
	    public String toString() {
	    	String status=getStatus();
	    	if(StringUtils.equals("New", status)){
		        return name;
	    	}else{
	    		return name + " (" + status + ")";
	    	}
	    }
	    
	    private String status=null;

		@Override
		public String getStatus() {
			if(status==null){
				boolean hasData=false;
				boolean needsData=false;
				for(VisitModalityI mod: getModalities()){
					String status= mod.getStatus();
					if(StringUtils.isEmpty(status)){
						needsData=true;
					}else if(StringUtils.equals("active", status)){
						needsData=true;
						hasData=true;
					}else{
						hasData=true;
					}
				}
				
				if(hasData && needsData){
					status= "Open";
				}else if(hasData){
					status= "Complete";
				}else{
					status= "New";
				}
			}
			
			return status;
		}

		@Override
		public void setSubject(Subject subject) {
			this.subject=subject;
			this.status=null;
			
			for(VisitModalityI vm: getModalities()){
				vm.setSubject(subject);
			}
		}

		@Override
		public Subject getSubject() {
			return subject;
		}
	}
	
	public static class VisitModality implements VisitModalityI{
		private final String           key;
		private final String           label;
		private final String           modality;
		private final String           display;
		private final Project          project;
		private final VisitOccurrenceI parent;
		private Subject subject=null;
		
		public VisitModality(String key, String label, String modality, String display, VisitOccurrenceI parent, Project project){
			this.label=label;
			this.modality=modality;
			this.display=display;
			this.key=key;
			this.parent=parent;
			this.project=project;
		}
		
		public VisitModality(JSONObject json, VisitOccurrenceI parent, Project project) throws JSONException {
			this(json.getString("key"),json.getString("label"),json.getString("type"),json.optString("display"),parent,project);
		}
		
		/* (non-Javadoc)
		 * @see org.nrg.util.VisitModalityI#getKey()
		 */
		@Override
		public String getKey() {
			return key;
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitModalityI#getLabel()
		 */
		@Override
		public String getLabel() {
			return label;
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitModalityI#getDisplay()
		 */
		@Override
		public String getDisplay() {
			return (StringUtils.isEmpty(display))?label:display;
		}

		/* (non-Javadoc)
		 * @see org.nrg.util.VisitModalityI#getModality()
		 */
		@Override
		public String getModality() {
			return modality;
		}
		
		private String status=null;
		
		public String getStatus(){
			if(status==null){
				String s;
				try {
					s = project.getSessionNamingConvention();
				} catch (Exception e) {
				    LoggerFactory.getLogger(VisitTemplate.class).error("",e);
					s=null;
				}
		    	if(s==null){
		    		s="{SUBJECT_LABEL}_{VISIT}_{MOD}";
		    	}
	
		    	s=StringUtils.replace(s, "{PROJECT}", project.toString());
		    	
		    	s=StringUtils.replace(s, "{SUBJECTID}", subject.getId());
		    	s=StringUtils.replace(s, "{SUBJECT_ID}", subject.getId());
		    	s=StringUtils.replace(s, "{SUBJECTLABEL}", subject.getLabel());
		    	s=StringUtils.replace(s, "{SUBJECT_LABEL}", subject.getLabel());
		    	
		    	s=StringUtils.replace(s, "{VISIT}", parent.getCode());
		    	s=StringUtils.replace(s, "{MOD}", this.getLabel());
		    	
				try {
					for(ProjectSession ps: project.getSessionLabels().getSessions()){
						if(StringUtils.equals(ps.getLabel(),s)){
							status= ps.getStatus();
							break;
						}
					}
				} catch (Exception ignored) {
				}
			}
			
			
			return status;
		}
	    		
		/*
	     * (non-Javadoc)
	     * @see java.lang.Object#toString()
	     */
	    public String toString() {	    	
	    	String status=getStatus();
	    	
	    	if(StringUtils.isEmpty(status)){
				return getDisplay();
	    	}else{
				if(StringUtils.equals(status, "active")){
					return getDisplay() + " (reupload)";
				}else{
					return getDisplay() + " (locked)";
				}
	    	}
	    }

		@Override
		public void setSubject(Subject subject) {
			this.subject=subject;
			this.status=null;
		}

		@Override
		public Subject getSubject() {
			return subject;
		}

		public boolean isEnabled() {
			final String status = getStatus();
			return StringUtils.isEmpty(status) || StringUtils.equals(status, "active");
		}
	}
}
