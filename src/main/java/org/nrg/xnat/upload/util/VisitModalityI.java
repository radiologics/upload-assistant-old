package org.nrg.xnat.upload.util;

import org.nrg.xnat.upload.data.Subject;

public interface VisitModalityI {

	String getKey();

	String getLabel();

	String getModality();
		
	void setSubject(Subject subject);
	
	Subject getSubject();
	
	boolean isEnabled();
	
	String getStatus();

	String getDisplay();

}
